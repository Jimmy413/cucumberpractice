package e2e_stepdefs;

import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.text.SimpleDateFormat;
import java.util.Date;


public class disclaimer_check {

    public static HashMap<String, String[]> disclaimerList = new HashMap<String, String[]>();

    public disclaimer_check() {
        disclaimerList.put("accounts/login.htm", new String[]{
                "Investments are not FDIC-insured, nor are they deposits of or guaranteed by a bank or any other entity, so they may lose value.",
                "Investors should carefully consider investment objectives, risks, charges and expenses. This and other important information is contained in the fund prospectuses and summary prospectuses, which can be obtained from a financial professional and should be read carefully before investing."
        });
        disclaimerList.put("service-and-support/forms/account-update-forms.html", new String[]{
                "Investments are not FDIC-insured, nor are they deposits of or guaranteed by a bank or any other entity, so they may lose value.",
                "Investors should carefully consider investment objectives, risks, charges and expenses. This and other important information is contained in the fund , which can be obtained from a financial professional and should be read carefully before investing.Investors should carefully consider investment objectives, risks, charges and expenses. This and other important information is contained in the fund  and ABLEAmerica Program Description, which can be obtained from a financial professional and should be read carefully before investing.",
                "Content contained herein is not intended to serve as impartial investment or fiduciary advice. The content has been developed by Capital Group, which receives fees for managing, distributing and/or servicing its investments."
        });
        disclaimerList.put("service-and-support/forms/retirement-savings-forms.html", new String[]{
                "Investments are not FDIC-insured, nor are they deposits of or guaranteed by a bank or any other entity, so they may lose value.",
                "Investors should carefully consider investment objectives, risks, charges and expenses. This and other important information is contained in the fund prospectuses and summary prospectuses, which can be obtained from a financial professional and should be read carefully before investing.",
                "Content contained herein is not intended to serve as impartial investment or fiduciary advice. The content has been developed by Capital Group, which receives fees for managing, distributing and/or servicing its investments."
        });
        disclaimerList.put("service-and-support/forms/college-savings-forms.html", new String[]{
                "Investments are not FDIC-insured, nor are they deposits of or guaranteed by a bank or any other entity, so they may lose value.",
                "Investors should carefully consider investment objectives, risks, charges and expenses. This and other important information is contained in the fund prospectuses, summary prospectuses and CollegeAmerica Program Description, which can be obtained from a financial professional and should be read carefully before investing. College America is distributed by American Funds Distributors, Inc. and sold through unaffiliated intermediaries. ",
                "Depending on your state of residence, there may be an in-state plan that provides state tax and other state benefits, such as financial aid, scholarship funds and protection from creditors, not available through CollegeAmerica. Before investing in any state's 529 plan, investors should consult a tax advisor.",
                "Content contained herein is not intended to serve as impartial investment or fiduciary advice. The content has been developed by Capital Group, which receives fees for managing, distributing and/or servicing its investments."
        });


    }

    public static WebDriver driver;

    public static String timeStamp;


    @Before ("@Disclaimer")
    public void config() {
        System.setProperty("webdriver.chrome.driver","chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        options.addArguments("--start-fullscreen");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    @After ("@Disclaimer")
    public void teardown() throws Throwable {

        TakesScreenshot ts = (TakesScreenshot) driver;
        File source = ts.getScreenshotAs(OutputType.FILE);
        timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        String destination = "target/cucumber-reports/"+ timeStamp + ".png";
        File finalDestination = new File(destination);
        FileUtils.copyFile(source, finalDestination);
        Reporter.addScreenCaptureFromPath(timeStamp + ".png");
        driver.close();
        driver.quit();
    }

    @Given("^individual page is (.*?)$")
    public void user_is_on_page(String page) throws Throwable {
        driver.get("https://www.americanfunds.com/individual/" + page);
    }

    @Then("^user should see the correct disclaimer for (.*?)$")
    public void correct_disclaimer_displayed(String page) throws Throwable {
        String[] pageDisclaimers = disclaimerList.get(page);
        WebElement element = driver.findElement(By.className("ns-disclosure"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);


        for (String disclaimer : pageDisclaimers){
            String actual = driver.findElement(By.cssSelector(".ns-disclosure")).getText();
            Assert.assertTrue(actual.contains(disclaimer));
        }

    }
}



