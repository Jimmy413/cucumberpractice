package e2e_stepdefs;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class americanfunds_login {
    public static WebDriver driver;
    @Given("^user is on the individual page$")
    public void user_is_on_homepage() throws Throwable {
        System.setProperty("webdriver.chrome.driver","chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://www.americanfunds.com/individual");
    }

    @When("^user clicks Login header link$")
    public void user_navigates_to_Login_Page() throws Throwable {
        driver.findElement(By.linkText("Login")).click();
    }

    @Then("^user is redirected to login page$")
    public void success_message_is_displayed() throws Throwable {
        String exp_message = "Log in to Your Account";
        String actual = driver.findElement(By.cssSelector(".accounts-headline")).getText();
        Assert.assertEquals(exp_message, actual);
        driver.quit();
    }
}
