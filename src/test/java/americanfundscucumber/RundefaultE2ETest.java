package americanfundscucumber;

import com.vimalselvam.cucumber.listener.ExtentProperties;
import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;

import cucumber.api.junit.Cucumber;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;


@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/defaultE2Efeature",
        glue = "defaultE2Estepdefs",
        plugin = { "com.vimalselvam.cucumber.listener.ExtentCucumberFormatter:"},
        monochrome = true
)

public class RundefaultE2ETest {
    @BeforeClass
    public static void setup() {
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        ExtentProperties extentProperties = ExtentProperties.INSTANCE;
        extentProperties.setReportPath("target/cucumber-reports/" + timeStamp + "_E2Ereport.html");
    }

    @After
    public static void writeExtentReport() {
        Reporter.loadXMLConfig(new File("config/report.xml"));
        Reporter.addScenarioLog("log");
    }


}


