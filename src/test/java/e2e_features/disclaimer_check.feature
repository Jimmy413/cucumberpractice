Feature: Verify page disclaimer
  Verify page disclaimer is correct

  @Disclaimer
  Scenario Outline: Verify page disclaimer
    Given individual page is <page>
    Then user should see the correct disclaimer for <page>


  Examples:
    | page  |
    | accounts/login.htm |
    | service-and-support/forms/account-update-forms.html |
    | service-and-support/forms/retirement-savings-forms.html |
    | service-and-support/forms/college-savings-forms.html |