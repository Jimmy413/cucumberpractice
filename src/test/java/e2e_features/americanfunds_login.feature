Feature: AmericanFunds Login Header link
  Verify user is redirected to login page

  Scenario: User clicks Login link in header
    Given user is on the individual page
    When user clicks Login header link
    Then user is redirected to login page